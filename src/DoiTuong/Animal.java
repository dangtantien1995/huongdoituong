package DoiTuong;

public class Animal {
	public String ten;
	public int soTuoi;
	
	public Animal(String ten, int soTuoi) {
		super();
		this.ten = ten;
		this.soTuoi = soTuoi;
	}

	public void TiengKeu() {
		System.out.println("Tiếng kêu của Animal");
	}
}
